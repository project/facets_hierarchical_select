/**
 * @file
 * Provides the Hierarchical Select Facet functionality.
 */

 (function ($) {

	'use strict';

	Drupal.facets = Drupal.facets || {};

	Drupal.behaviors.facet_hierarchical_select = {
		attach: function (context, settings) {
			Drupal.facets.dropdowns = [];
			$('.js-facets-hierarchy-links').once('facets-hierarchy-transform').each(function () {
				var element = $(this);
				Drupal.facets.iterateDropdowns($(this).children('.facet-item'), 0);
				for (var i = 0; i < Drupal.facets.dropdowns.length; i++) {
					element.parent().append(Drupal.facets.dropdowns[i]).on('change.facets', function () {
						var anchor = element.find("[data-drupal-facet-item-id='" + $(this).find(':selected').data('drupalFacetItemId') + "']");
						var linkElement = (anchor.length > 0) ? $(anchor) : items.find('.default-option a');
						var url = linkElement.attr('href');
						console.log(url);
						$(this).trigger('facets_filter', [ url ]);
					});
				}
				console.log(element.parent()[0]);
				Drupal.attachBehaviors(element.parent()[0], Drupal.settings);
			});


		}
	};

	Drupal.facets.iterateDropdowns = function(items, index) {
		if(items.length) {
			Drupal.facets.dropdowns[index] = $('<select class="js-facets-widget"></select>');
			// Add empty text option first.
			var default_option = $('<option></option>')
				.attr('value', '')
				.text('default_option_label');
			Drupal.facets.dropdowns[index].append(default_option);

			items.each(function() {
				if($(this).find('ul')) {
					var children = $(this).find('ul').children('.facet-item');
					Drupal.facets.iterateDropdowns(children, index+1);
				}

				var link = $(this).children('a');
				var href = link.attr('href').split('?')[0];
				var facet_id = link.attr('data-drupal-facet-item-id').split('-');
				facet_id.pop();
				console.log(facet_id);
				var facet_value = link.attr('data-drupal-facet-item-value');
				var active = link.hasClass('is-active') || $(this).hasClass('facet-item--active-trail');
				var option = $('<option></option>')
					.attr('value', href + '?' + facet_id.join('_') + '=' + facet_value)
					.data(link.data());


				if (active) {
					// Set empty text value to this link to unselect facet.
					default_option.attr('value', link.attr('href'));
					items.find('.default-option a').attr("href", link.attr('href'));
					option.attr('selected', 'selected');
					link.find('.js-facet-deactivate').remove();
				}
				option.text(link.text().trim());

				// Go to the selected option when it's clicked.
				Drupal.facets.dropdowns[index].append(option);
			});

			Drupal.facets.dropdowns[index].on('change.facets', function () {
				var anchor = $(items).find("[data-drupal-facet-item-id='" + $(this).find(':selected').data('drupalFacetItemId') + "']");
				var linkElement = (anchor.length > 0) ? $(anchor) : items.find('.default-option a');
				var url = linkElement.attr('href');
				console.log(url);
				$(this).trigger('facets_filter', [ url ]);
			});
		}
	}
})(jQuery);

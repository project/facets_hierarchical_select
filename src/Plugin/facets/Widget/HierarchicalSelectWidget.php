<?php

namespace Drupal\facets_hierarchical_select\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Plugin\facets\widget\LinksWidget;
use Drupal\facets\Widget\WidgetPluginBase;

/**
 * The Hierarchical Select widget.
 *
 * @FacetsWidget(
 *   id = "hierarchical_select",
 *   label = @Translation("Hierarchical select"),
 *   description = @Translation("A configurable widget that shows a hierarchical select facet."),
 * )
 */
class HierarchicalSelectWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'depth' => '1',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet) {
    $build = parent::build($facet);

    // Attaching Drupal Settings.
    // @todo: Attach JS library.
    $build['#attributes']['class'][] = 'js-facets-hierarchy-links';
    $build['#attached']['drupalSettings']['facets']['hierarchical_select_widget'][$facet->id()]['deepest-level'] = $this->getConfiguration()['depth'];
    $build['#attached']['library'][] = 'facets_hierarchical_select/hierarchical_select';
    $build['#attached']['library'][] = 'facets/drupal.facets.general';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $config = $this->getConfiguration();

    $form += parent::buildConfigurationForm($form, $form_state, $facet);

    $form['depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of levels to display'),
      '#default_value' => $config['depth'],
      '#description' => $this->t('This maximum number includes the initial level.'),
      '#min' => 1,
    ];

    return $form;
  }

}

Facets Hierarchical Select
===========
A configurable widget that shows a hierarchical select facet.


Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8


Configuration
------------


